'use strict';

var entryFactory = require('bpmn-js-properties-panel/lib/factory/EntryFactory');

var is = require('bpmn-js/lib/util/ModelUtil').is;

module.exports = function(group, element) {

  // Only return an entry, if the currently selected
  // element is a start event.

  if (is(element, 'bpmn:ChoreographyActivity') || is(element, 'bpmn:ChoreographyActivity2')) {
    group.entries.push(entryFactory.textField({
      id : 'participantA',
      label : 'ParticipantA',
      modelProperty : 'participantA'
    }));
    group.entries.push(entryFactory.textField({
      id : 'participantB',
      label : 'ParticipantB',
      modelProperty : 'participantB'
    }));
  }
};